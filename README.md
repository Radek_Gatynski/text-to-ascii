## Page to check ascii code
On this page you can check ascii code, all what you need to do is open file index.html in your browser.

## Used technology
HTML, SCSS, JavaScript

## How to use
The connection with network is necessary because we need bootstrap.
To run this please open index.html file in your browser.
We have three fields:
The first on left site convert to ascii number.
On right site convert decimal number into sign and Hex sign.
On the bottom we give the text and in the results we will get text writen in decimal system.