let to_ascii = document.getElementById('to_ascii');
let resultCode = document.getElementById('resultCode');
let textToTranslate = document.getElementById('textToTranslate');
let translatedText = document.getElementById('translatedText');
let convertToSign = document.getElementById('convertToSign');

to_ascii.onkeydown = function(evt) {
    evt = evt || window.event;
    resultCode.innerText = evt.keyCode;
};


convertToSign.addEventListener('click', function(){
    let convertValue = document.getElementById('inputCode').value;
    if(convertValue){
        let result_signChar = document.getElementById('resultSignChar');
        result_signChar.innerText = String.fromCharCode(convertValue);
        let result_signHex = document.getElementById('resultSignHex');
        result_signHex.innerText = Number(convertValue).toString(16).toUpperCase();
    }
})


if (textToTranslate.addEventListener) {
    textToTranslate.addEventListener('input', function() {
        let textToPut = '';
        for(let i=0;i<textToTranslate.value.length;i++){
            textToPut += textToTranslate.value.charCodeAt(i) + ' ';
        }
        translatedText.innerText = textToPut;
    }, false);
  } else if (textToTranslate.attachEvent) {
    textToTranslate.attachEvent('onpropertychange', function() {
        let textToPut = '';
        for(let i=0;i<textToTranslate.value.length;i++){
            textToPut += textToTranslate.value.charCodeAt(i) + ' ';
        }
        translatedText.innerText = textToPut;
    });
  }